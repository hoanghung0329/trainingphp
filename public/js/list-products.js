//get product checked
var listProducts = {};
$('.product-checkbox').change(function(){
	var productCode = $(this).attr('data-product-code');
	var productName = $(this).attr('data-product-name');
	if($(this).is(':checked')){
		if(!listProducts[productCode])
			listProducts[productCode] = productName;
	}else{
		if(listProducts[productCode] && listProducts[productCode].length != 0)
			delete listProducts[productCode];
	}
})
// update status
$('.btn-modal').click(function(){
	var listProductCode = [];
	var listUpdate = {};

	$('.list-products').html('');
	for(product in listProducts){
		$('.list-products').append('<li><span class="label label-default">'+listProducts[product]+'</span></li>');
		listProductCode.push(product);
	}
	listUpdate.listProductCode = listProductCode;
	listUpdate.status = $(this).attr('data-type-status');
	if(Object.keys(listProducts).length === 0)
		$('.submit-update').attr('disabled','disabled');
	else
		$('.submit-update').prop('disabled', false);
	$('.list-update').val(JSON.stringify(listUpdate));
});
// clear button
$('#clear-search-btn').click(function(){
	$('.search-button').val('');
});
