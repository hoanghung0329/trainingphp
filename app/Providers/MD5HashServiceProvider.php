<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Lib\MD5Hasher;

class MD5HashServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
    * Register the service provider.
    *
    * @return void
    */
    public function register() {
        $this->app->singleton('hash', function($app)
        {
            return new MD5Hasher;
        });
    }

    /**
    * Get the services provided by the provider.
    *
    * @return array
    */
    public function provides() {
        return array('hash');
    }

}
