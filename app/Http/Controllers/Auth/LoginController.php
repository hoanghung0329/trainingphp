<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Cookie;
use App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Username Customization
     * @return [type] [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function username()
    {
        return 'user_name';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function index(Request $request)
    {
        if(!empty($request->cookie('remember_login'))){
            $token = $request->cookie('remember_login');
            $user = User::where('remember_token', '=', $token)->first();
            if($user){
                Auth::loginUsingId( $user->t_admin_id);
                Auth::user()->update([
                    'last_login_at' => Carbon::now()->toDateTimeString(),
                    'last_login_ip' => $request->getClientIp()
                ]);
                return redirect()->action('AppController@index');
            }
        }
        return view('login');
    }
    /**
     * Login by username and password
     * @param  Request $request [description]
     * @return [view]           [Admin view]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function checkLogin(Request $request){
       
        $validatedData = $request->validate([
            'username' => 'required|min:3|alpha_num',
            'password' => 'required|min:3|alpha_num'
        ]);
        $username = $request->input('username');
        $password = $request->input('password');

        $credentials = array(
                    'user_name' =>$username,
                    'password' => $password
                );

        $user = Auth::attempt($credentials);

        if($user){
            if($request->input('remember') == 1){
                $token = Str::random(60);
                Cookie::queue('remember_login', $token , 24*30*60);
            }
            Auth::user()->update([
                'remember_token' => isset($token)?$token:'',
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]);
            return redirect()->action('AppController@index');
        }
        else
            return view('login',array('errorLogin'=>'Cannot find username, please check username or password'));       
    }
}
