<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;

class AppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function index(Request $request)
    {
        $condition = array();
        if( !empty($request->input('search')) ){
           $condition = $request->input('search');
        }
        $listProducts = Product::getProductByAuthId($condition);
        $msg = !empty($msg) ? $msg : '' ;
        return view('welcome',['listProducts'=>$listProducts,'condition'=>$condition,'msg'=>$msg]);
    }

    /**
     * update status list product
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function updateStatus(Request $request){
        $listUpdate = $request->input('list-update');
        if(!empty($listUpdate)){
            $listUpdate = json_decode($listUpdate,true);
            $products = Product::getProductByList($listUpdate['listProductCode']);
            $msg = '';
            foreach ($products as $productCode => $product) {
                $result = Product::updateStatus($products[$productCode],$listUpdate['status']);
                if(!$result)
                    $msg = 'Error update status';
                else
                    $msg = 'Update success';
            }
        }
        Session::flash('message',$msg); 
        return redirect()->action('AppController@index');
    }

    /**
     * Export csv
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function exportCsv(Request $request){
        $condition = $request->input('condition');
        $dataHeader = array('ステータス', 'メーカー名', '品番', 'JANコード', '商品名','定価','処理ステータス');
        $dataContent = array();
        Product::getProductByAuthId($condition,true)->chunk(1000000,function($products) use(&$dataContent){
            foreach ($products as $key => $product) {
                //ステータス
                if($product->cheetah_status === 0)
                    $dataContent[$key]['cheetah_status'] = '販売';
                elseif($product->cheetah_status === 0)
                    $dataContent[$key]['cheetah_status'] = '欠品';
                elseif($product->cheetah_status === 0)
                    $dataContent[$key]['cheetah_status'] = '廃盤';
                else
                    $dataContent[$key]['cheetah_status'] = 'null';
                //メーカー名   
                $dataContent[$key]['maker_full_nm'] = $product->maker_full_nm;
                //品番
                $dataContent[$key]['product_code'] = $product->product_code;
                //JANコード 
                $dataContent[$key]['product_jan'] = $product->product_jan;
                //商品名 
                $dataContent[$key]['product_name'] = $product->product_name;
                //定価 
                $dataContent[$key]['list_price'] = $product->list_price;
                //処理ステータス
                if($product->process_status === 0)
                    $dataContent[$key]['process_status'] = 'Default';
                elseif($product->process_status === 1)
                    $dataContent[$key]['process_status'] = 'Waitting';
                elseif($product->process_status === 2)
                    $dataContent[$key]['process_status'] = 'Approved';
                else
                    $dataContent[$key]['process_status'] = 'null';
            }
        });  
        $this->formatCsv($dataHeader,$dataContent);
        die;
    }

    /**
     * Format csv
     * @param  [type] $dataHeader  [description]
     * @param  [type] $dataContent [description]
     * @return [type]              [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function formatCsv($dataHeader,$dataContent){
           // output headers so that the file is downloaded rather than displayed
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="demo.csv"');
         
        // do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');
         
        // create a file pointer connected to the output stream
        $file = fopen('php://output', 'w');
         
        // send the column headers
        fputcsv($file,$dataHeader );
         
        // output each row of the data
        foreach ($dataContent as $row)
        {
        fputcsv($file, $row);
        }
    }
    /**
     * Import CSV
     * @param  [type] $request  [Request]
     * @return [type] AppController [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public function importCsv(Request $request){
        if( $_FILES['importCsv']){
            $file = $_FILES['importCsv'];
            $fileContent = fopen($file['tmp_name'], 'r');
            $data = array();
            while(! feof($fileContent)){
                $data[] = fgetcsv($fileContent);                
            }
            fclose($fileContent);
            $productChunk = collect($data)->chunk(100);
            unset($productChunk[0][0]);
            foreach ($productChunk as $key => $chunk) {
                foreach ($chunk as $product) {
                    Product::insertProductFromCsv($product);
                }
            }
            return redirect()->action('AppController@index');
        }
    }
}
