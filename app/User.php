<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_users';
    /**
     * The table with other primary key
     * @var string
     */
    protected $primaryKey = 't_admin_id';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_name','password','remember_token','last_login_at','last_login_ip'];

}
