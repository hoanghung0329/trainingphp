<?php    
namespace App\Lib;        
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Hashing\AbstractHasher;

class MD5Hasher extends AbstractHasher implements HasherContract {

	public function make($value, array $options = array()) {
		$value = env('SALT', '').$value;
		return md5($value);
	}

	public function check($value, $hashedValue, array $options = array()) {
		return $this->make($value) === $hashedValue;
	}

	public function needsRehash($hashedValue, array $options = array()) {
		return false;
	}

}