<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_product';
    /**
     * The table with other primary key
     * @var string
     */
    protected $primaryKey = 'product_code';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_code','price_supplier_id','cheetah_status','','last_login_ip'];
     /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * Set timestamps
     * @var boolean
     */
    public $timestamps =false;
    /**
     * Get product by auth id
     * @return [type]             [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public static function getProductByAuthId($condition = array(), $getAll = false){
    	$supplierId = Auth::id();
    	if(!empty($supplierId)){
    		$result = Product::select('cheetah_status','maker_full_nm','product_code','product_jan','product_name','list_price','process_status')->where('price_supplier_id','=',$supplierId);
            if(!empty($condition) && is_array($condition)){
                foreach ($condition as $col => $val) {
                    if($val){
                        $query = explode(" ",trim($val));
                        $result->whereIn($col,$query);
                    }
                }
            }
            if(!$getAll)
    		    return $result->paginate(50);
            else
                return $result;
    	}
    }

    /**
     * Get product by list
     * @param  [array] $productCodes [list of product]
     * @return [array]               [list product in database]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public static function getProductByList($productCodes){
        $result = array();
        foreach ($productCodes as $productCode) {
            $product = Product::find($productCode);
            if(!empty($product)){
                $result[$productCode] = $product;
            }
        }
        return $result;
    }

    /**
     * Update status
     * @param  [Object] $product    [description]
     * @param  [String] $statusType [description]
     * @return [type]             [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public static function updateStatus ($product,$statusType){
        if(!empty($product) && isset($statusType)){
            if($statusType == 0){
                $product->cheetah_status = 2;
                $product->process_status = 1;
            }elseif( $statusType == 1){
                $product->cheetah_status = 1;
                $product->process_status = 1;
            }elseif( $statusType == 2){
                $product->cheetah_status = 0;
                $product->process_status = 1;
            }
            return $product->save();
        }
        return false;
    }
    /**
     * Insert product from csv
     * @param  [array] $data [Data product]
     * @return [type]       [description]
     */
    # Author :hoang.hung (hoang.hung.rcvn2012@gmail.com)
    public static function insertProductFromCsv($data){
        try{
            $product = new Product();
            if($data[0] === '販売')
                $product->cheetah_status = 0;
            elseif($data[0] === '欠品')
                $product->cheetah_status = 1;
            elseif($data[0] === '廃盤') 
                $product->cheetah_status = 2;
            else 
                $product->cheetah_status = 0;

            $product->maker_full_nm = !empty($data[1])?"$data[1]":'';
            $product->product_code = !empty($data[2])?"$data[2]":'';
            $product->product_jan = !empty($data[3])?"$data[3]":'';
            $product->product_name = !empty($data[4])?"$data[4]":'';
            $product->list_price = !empty($data[5])?(int)$data[5]:0;

            if($data[6] === 'Default')
                $product->process_status = 0;
            elseif($data[6] === 'Waitting')
                $product->process_status = 1;
            elseif($data[6] === 'Approved')
                $product->process_status = 2;
            else
                $product->process_status = 0;
            return $product->save();
        }catch(Exception $e){
            throw $e;
        }
    }
}
