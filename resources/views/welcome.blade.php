@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/list-products.css') }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/list-products.js') }}"></script>
@endsection

@section('admin-login')
    <b>{{ Auth::user()->user_name}}</b>
@endsection

@section('search')
    {{-- search view --}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">廃盤 - 欠品 - 販売</h3>
                    <h3 class="text-info">{{ (Session::has('message'))? Session::get('message'):'' }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{ url('/') }}" method="get">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <input type="text" name="search[cheetah_status]" class="form-control search-button" value="{{ isset($condition['cheetah_status'])?$condition['cheetah_status']:'' }}"/>
                                    <label for="cheetah_status"> ステータス </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <input type="text" name="search[product_jan]" class="form-control search-button" value="{{ isset($condition['product_jan'])?$condition['product_jan']:'' }}" />
                                    <label for="jan"> JANコード </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="input-group">
                                     <input type="text" name="search[maker_full_nm]" class="form-control search-button" value="{{ isset($condition['maker_full_nm'])?$condition['maker_full_nm']:'' }}"/>
                                     <label for="maker_full_nm"> メーカー名 </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <input type="text" name="search[product_code]" class="form-control search-button" value="{{ isset($condition['product_code'])?$condition['product_code']:'' }}">
                                    <label for="product_code"> 品番 </label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <input type="text" name="search[product_name]" class="form-control search-button" value="{{ isset($condition['product_name'])?$condition['product_name']:'' }}">
                                    <label for="product_name"> ステータス </label>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <input type="submit" name="" class="btn btn-primary" value="検索する">
                                <button type="button"  class="btn btn-primary" id="clear-search-btn" >元に戻す</button>
                            </div>
                        </div>
                        
                    </form>

                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-5  col-md-push-7">
                           <button type="button" class="btn btn-danger btn-modal"  data-toggle="modal" data-target="#myModal" data-type-status ="0">廃盤にする</button>
                           <button type="button"  class="btn btn-warning btn-modal"  data-toggle="modal" data-target="#myModal" data-type-status ="1">欠品にする</button>
                            <button type="button" class="btn btn-success btn-modal" data-toggle="modal" data-target="#myModal" data-type-status ="2">販売にする</button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal update -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">商品</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span class="label label-primary">廃番</span> 下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。<br>
                        <span class="label label-danger">廃番</span>にしない場合は、閉じる ボタンを押してください。
                    </p>
                    <ul class="list-products">
                        
                    </ul>
                </div>
                <div class="modal-footer">
                    <form action="{{ url('/updateStatus') }}" method="post" >
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                        <input type="hidden" name="list-update" class="list-update">
                        <input type="submit" class="btn btn-primary submit-update" value="確定">
                    </form>         
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Modal import -->
    <div class="modal fade" id="myModalImport" tabindex="-1" role="dialog" aria-labelledby="myModalImporLabel">
        <div class="modal-dialog" role="document">
            <form action="{{ url('/importCsv') }}" method="post"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalImporLabel">商品</h4>
                    </div>
                    <div class="modal-body">
                        <input type="file" name="importCsv" class="form-control" >
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Import">
                            
                    </div>
                </div>
            </form>    
        </div>
    </div>
    {{-- Manager product --}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Quan ly san pham</h3>
                    <div class="row pull-right">
                        <a class="btn btn-primary" href="{{action('AppController@exportCsv',['condition' => $condition] ) }}">Export CSV</a>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalImport" data-type-status ="2">Import CSV</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row" class="bg-primary">
                                            <th>ステータス</th>
                                            <th>メーカー名</th>
                                            <th>品番</th>
                                            <th>JANコード</th>
                                            <th>商品名</th>
                                            <th>定価</th>
                                            <th>処理ステータス</th>
                                            <th>出荷指示+全て選択-選択解除</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listProducts as $product)
                                            <tr role="row" class="even">
                                                {{-- ステータス --}}
                                                @if($product->cheetah_status === 0)
                                                    <td><span class="label label-success">販売</span></td>
                                                @elseif($product->cheetah_status === 1)
                                                    <td><span class="label label-warning">欠品</span></td>
                                                @elseif($product->cheetah_status === 2)
                                                    <td><span class="label label-danger">廃盤</span></td>
                                                @else
                                                    <td></td>
                                                @endif
                                                {{-- メーカー名 --}}
                                                <td>{{ $product->maker_full_nm }}</td>
                                                {{-- 品番 --}}
                                                <td>{{ $product->product_code }}</td>
                                                {{-- JANコード --}}
                                                <td>{{ $product->product_jan }}</td>
                                                {{-- 商品名 --}}
                                                <td>{{ $product->product_name }}</td>
                                                {{-- 定価 --}}
                                                <td>{{ $product->list_price?:0 }}</td>
                                                {{-- 処理ステータス --}}
                                                @if($product->process_status === 0)
                                                    <td>Default</td>
                                                @elseif($product->process_status === 1)
                                                    <td>Waitting</td>
                                                @elseif($product->process_status === 2)
                                                    <td>Approved</td>
                                                @else
                                                    <td></td>
                                                @endif
                                                {{-- 出荷指示+全て選択-選択解除 --}}
                                                <td>
                                                    <input type="checkbox" name="" class="product-checkbox" data-product-name="{{ $product->product_name }}" data-product-code="{{ $product->product_code}}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $listProducts->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@endsection
