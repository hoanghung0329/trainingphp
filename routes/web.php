<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
	Route::get('/', 'AppController@index');
	Route::get('/exportCsv', 'AppController@exportCsv');
	Route::post('/importCsv', 'AppController@importCsv');
	Route::post('/updateStatus', 'AppController@updateStatus');
});
Route::get('/login', 'Auth\LoginController@index');
Route::post('/login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@checkLogin'] );